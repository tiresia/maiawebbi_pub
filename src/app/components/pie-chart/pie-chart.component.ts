import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/internal/operators/map';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {
  @ViewChild(BaseChartDirective)
  public chart!: BaseChartDirective;

  @Input() data: any;
  @Input() height!: number;
  @Input() indicator:any;
  pixelsChart!: number;
  responsive: boolean = false;

  pieChartOptions!: ChartOptions;
  pieChartLabels: Label[] = [];
  pieChartData: number[] = [];
  pieChartType: ChartType = 'pie';
  pieChartLegend = true;
  pieChartPlugins = [pluginDataLabels] as any;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(response => response.matches)
    );


  constructor(private breakpointObserver:BreakpointObserver) {

  }

  ngOnInit() {
    let keys = [] as any;
    let values = [] as any;
    let percentages = [] as any;
    this.data.map((object:any, index:any) => {
      let keysObject = Object.keys(object);
      keysObject.map((key)=>{
        if(key != this.indicator.titleField && key != 'Porcentaje'){
          values.push(object[key]);
        }else if(key == 'Porcentaje'){
          percentages.push(object[key]);
        }
      });
      let name = String(this.data[index][this.indicator.titleField]).toLowerCase();
      let nameCapitalized = name.charAt(0).toUpperCase() + name.slice(1)
      keys.push(nameCapitalized);
    });

    this.pieChartData = values;

    this.pieChartLabels = keys;
    
    this.pieChartOptions = {
      responsive: true,
      tooltips:{
        callbacks:{
          title: function (tooltipItem, data) {
            return (data['labels']![tooltipItem[0]['index']!]).toLocaleString();
          },
          label: function (tooltipItem, data) {
            return Number(data['datasets']![0]['data']![tooltipItem['index']!]).toLocaleString();
          },
          afterLabel: function (tooltipItem, data) {
            return percentages[tooltipItem['index']!];
          }
        }
      },
      animation:{
        animateScale:true
      },
      legend: {
        position: 'left',
      },
      plugins: {
        datalabels: {
          formatter: (value, ctx) => {
            const label = percentages[ctx.dataIndex];
            return label;
          },
        },
      }
    }

   
    let widthWindow = screen.width;
    let heightWindow = screen.height;
    let pixels = 0;
    if (!this.responsive) {
      if (widthWindow > heightWindow) {
        pixels = (this.height / 100) * widthWindow;
      } else {
        pixels = (this.height / 100) * heightWindow;
      }
      this.pixelsChart = (pixels - 20);
    } else {
      if (widthWindow > heightWindow) {
        let result = (this.height / 100) * widthWindow;
        if (result > widthWindow) {
          pixels = result - widthWindow;
        } else {
          pixels = widthWindow - result;
        }
      } else {
        let result = (this.height / 100) * heightWindow;
        if (result > heightWindow) {
          pixels = result - heightWindow;
        } else {
          pixels = heightWindow - result;
        }
      }
      this.pixelsChart = (pixels + 20);
    }
  }

  isResponsive(responsive:boolean){
    this.responsive = responsive;
    if(responsive){
      this.pieChartOptions.legend!.position = 'left';
    }
  }

}
