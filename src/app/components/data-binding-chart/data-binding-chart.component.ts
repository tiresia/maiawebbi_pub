import { Component, OnInit, Input} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';


@Component({
  selector: 'app-data-binding-chart',
  templateUrl: './data-binding-chart.component.html',
  styleUrls: ['./data-binding-chart.component.css']
})
export class DataBindingChartComponent implements OnInit {
  @Input() data: any;
  @Input() height!: number;
  @Input() indicator: any;
  @Input() keys: any;

  barChartOptions!: ChartOptions;
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = false;
  barChartPlugins = [pluginDataLabels as any];
  barChartData: ChartDataSets[] = [];

  responsive: boolean = false;
  pixelsChart?: number;
  pixel?:number;

  constructor() { }

  ngOnInit() {
    this.barChartOptions = {
      responsive: true,
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            var label = data!.datasets![tooltipItem.datasetIndex!].label || '';

            if (label) {
              label += ': ';
            }
            label += Number(tooltipItem.yLabel).toLocaleString();
            return label;
          }
        }
      },
      scales: {
        xAxes: [{
          gridLines: {
            offsetGridLines: true
          },
        }],
        yAxes: [{
          gridLines: {
            offsetGridLines: true
          },
          ticks: {
            callback: function (number, index, values) {
              if (isNaN(Number(number))) return null;
              if (number === null) return null;
              if (number === 0) return null;
              let abs = Math.abs(Number(number));
              const rounder = Math.pow(10, 1);
              const isNegative = number < 0;
              let key = '';

              const powers = [
                { key: 'Q', value: Math.pow(10, 15) },
                { key: 'T', value: Math.pow(10, 12) },
                { key: 'B', value: Math.pow(10, 9) },
                { key: 'M', value: Math.pow(10, 6) },
                { key: 'K', value: 1000 }
              ];

              for (let i = 0; i < powers.length; i++) {
                let reduced = abs / powers[i].value;
                reduced = Math.round(reduced * rounder) / rounder;
                if (reduced >= 1) {
                  abs = reduced;
                  key = powers[i].key;
                  break;
                }
              }
              return (isNegative ? '-' : '') + abs + key;
            }
          }
        }]
      },
      plugins: {
        datalabels: {
          formatter: (value, ctx) => {
            return "";
          },
        },
      }
    };

    this.keys.map((key:any) => {
      let object = {
        label: key,
        data: [] as any,
      }
      this.data.map((data:any) => {
        if (key != this.indicator.titleField) {
          if (Number(data[key])) {
            object.data.push(data[key]);
          }
        }
      });

      if(object.data.length > 0){
        this.barChartData.push(object);
      }
    });

    this.data.map((data:any) => {
      this.barChartLabels.push(data[this.indicator.titleField]);
    })
  }

}
