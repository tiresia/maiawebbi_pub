import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataBindingChartComponent } from './data-binding-chart.component';

describe('DataBindingChartComponent', () => {
  let component: DataBindingChartComponent;
  let fixture: ComponentFixture<DataBindingChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataBindingChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataBindingChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
