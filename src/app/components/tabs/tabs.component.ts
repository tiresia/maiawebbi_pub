import { Component,ContentChildren,QueryList,AfterContentInit,ViewChild} from '@angular/core';
import { TabComponent } from './tab/tab.component';
import { DynamicTabsDirective } from './tab/dynamic-tabs.directive';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements AfterContentInit {
  dynamicTabs: TabComponent[] = [];
  @ContentChildren(TabComponent) tabs?: QueryList<TabComponent>;
  @ViewChild(DynamicTabsDirective) dynamicTabPlaceholder?: DynamicTabsDirective;
  constructor() { }

  ngOnInit() {
  } 
  ngAfterContentInit() {
      // get all active tabs
      const activeTabs = this.tabs?.filter(tab => tab.active);
      // if there is no active tab set, activate the first
      if (activeTabs?.length === 0) {
        this.selectTab(this.tabs!.first);
      }
    }
  selectTab(tab: TabComponent) {
    // deactivate all tabs
    this.tabs!.toArray().forEach(tab => (tab.active = false));
    this.dynamicTabs.forEach(tab => (tab.active = false));
    // activate the tab the user has clicked on.
    tab.active = true;
  }
}
