import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { parseString } from 'xml2js';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/internal/operators/map';
import { RepositoryService } from 'src/app/services/repository.service';

@Component({
  selector: 'app-content-dashboard',
  templateUrl: './content-dashboard.component.html',
  styleUrls: ['./content-dashboard.component.css']
})
export class ContentDashboardComponent implements OnInit {

  @Input() tab: any;
  @Input() typeGraphicsDashboard: any;
  user: any;
  indicators: any[] = [];
  objectLabels:any = {};
  classContent: string = 'content_indicator';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(response => response.matches)
    );

  constructor(
    private repository: RepositoryService,
    private breakpointObserver: BreakpointObserver,
    private cdRef: ChangeDetectorRef
  ) {
    this.user = JSON.parse(localStorage.getItem('user')!);
  }
  

  ngOnInit() {
    console.log('content-dash');
    let indicatorsContent = [] as any;
    let indicators = this.tab.indicators;
    let rowsIndicators = [] as any;

    for (let key in indicators) {
      let indicator = indicators[key];
      indicatorsContent.push(indicator);
    }

    indicatorsContent = indicatorsContent.sort(function (a:any, b:any) {
      return parseFloat(a.orden) - parseFloat(b.orden);
    });

    indicatorsContent.map((indicator:any) => {
      rowsIndicators.push(indicator.row);
    });

    if (rowsIndicators.length > 0) {
      let rows = rowsIndicators.filter((row:any, index:any) => {
        return rowsIndicators.indexOf(row) == index;
      });
      let array = [] as any;
      rows.map((row:any) => {
        let dataRow = {
          row: row,
          columns: [] as any,
          count: 0
        }
        indicatorsContent.map((indicator:any) => {
          if (indicator.row == row) {
            indicator.progress = true;
            indicator.class = "flex" + indicator.x;
            indicator.rowClass = "row" + indicator.y;
            
           
            let numberY = Number(indicator.y);
            let heightGrid = "0%"
            let chart  = 0;
            switch (numberY) {
              case 1:
                heightGrid = "15%";
                chart = 15;
                break;
              case 2:
                heightGrid = "30%";
                chart = 30;
                break;
              case 3:
                heightGrid = "45%";
                chart = 45;
                break;
              case 4:
                heightGrid = "60%";
                chart = 60;
                break;
              case 5:
                heightGrid = "75%";
                chart = 75;
                break;
              case 6:
                heightGrid = "90%";
                chart = 90;
                break;
            };
            indicator.heightGrid = heightGrid;
            indicator.heightGridChart = chart;
            dataRow.columns.push(indicator);
          }
        });

        if (dataRow.columns.length > 0) {
          dataRow.count = dataRow.columns.length;
        }
        array.push(dataRow);
      });
      this.indicators = array;
    }

    console.log(this.indicators);
    this.loadIndicatorsRepository();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  loadIndicatorsRepository() {
    this.indicators.map((indicator) => {
      console.log(indicator);
      if(indicator.columns.length > 0){
        indicator.columns.map((column:any) => {
          let data = {
            sesId: this.user.sesId,
            type: column.type,
            dkIndicator: column.Dkindicador,
            consult: column.consult,
            username:this.user.username
          };

          let errors = "";
          this.repository.getIndicators(data).subscribe((response) => {
            this.responseIndicator(response, column);
          },
            (error) => {
              errors = error.message;
              console.log("error");
              console.log(errors);
            });
        })
      }else{
        indicator['progress'] = false;
        indicator['success'] = '';
        indicator['error'] = "No hay datos para mostrar"; 
      }
    });
  }

  responseIndicator(response: any, indicator: any) {
    let errors = "";
    let responseSuccess = "";

    parseString(response, function (error: any, result: any) {
      if (result) {
        if (result.main.status == 'ok') {
          if(result.main.data != "[]" && result.main.data != undefined){
            result.main.campos = JSON.parse(result.main.campos);
            let jsonData = JSON.parse(result.main.data);
            let array = [];
            for (let key in jsonData) {
              array.push(jsonData[key]);
            }
            result.main.data = JSON.parse(JSON.stringify(array));

            if (indicator.tipografica == 'rotatedAxisLabels') {
              let results = result.main.data;
              let arrayKeys = [] as any;
              results.map((result: any, index: number) => {
                let keys = Object.keys(result);
                arrayKeys = keys;
                results[index][indicator.valueField] = Number(result[indicator.valueField]);
              });
              let keys = [] as any;
              arrayKeys.map((key:any) => {
                if (key != indicator.titleField) {
                  keys.push(key);
                }
              })
              result.main.keys = keys;
            }

            if (indicator.tipografica == 'simplePieChart'){
              let results = result.main.data;
              let sum = 0;
              results.map((res:any) => {
                let value = Number(res[indicator.valueField]);
                sum = sum+value; 
              });

              results.map((result: any, index: number) => {
                results[index][indicator.valueField] = Number(result[indicator.valueField]);
                results[index]['Porcentaje'] = ((Number(result[indicator.valueField]) / sum) * 100).toFixed(2) + '%';
                indicator.porcentaje = 'Porcentaje';
              });

            }

            if (indicator.tipografica == 'stackedBarChart' || indicator.tipografica == 'chartWithLegend' || indicator.tipografica == 'dataBinding'){
              let results = result.main.data;
              let arrayKeys = [] as any;
              results.map((result: any, index: number) => {
                let keys = Object.keys(result);
                arrayKeys = keys;
                keys.map((key, i)=>{
                  if(keys[i] != indicator.titleField){
                    let letters = result[key].match(/^[A-Za-z]+$/);
                    if(!letters){
                      if (Number(result[key])) {
                        results[index][key] = Number(result[key]);
                      }
                    }else{
                      let numberString = String(result[key]).replace('$', '');
                      numberString = numberString.replace('M', '');
                      if(Number(numberString)){
                        results[index][key] = Number(numberString);
                      }
                    }
                  }
                });
              });

              let keys = [] as any;
              arrayKeys.map((key:any)=>{
                if(key != indicator.titleField){
                  keys.push(key);
                }
              })
              result.main.keys = keys;
            }

            if (indicator.tipografica == 'valor') {
              let number = new Intl.NumberFormat().format(result.main.data[0][result.main.campos]);
              result.main.data = number;
            }
            responseSuccess = result.main;
            //console.log('datos',result.main);
          }else{
            errors = "No hay datos para mostrar";
          }
        } else if (result.main.status == 'error') {
          errors = result.main.error;
        }
      } else {
        errors = error;
      }
    });

    if (!errors) {
      indicator['progress'] = false;
      indicator['success'] = responseSuccess;
      indicator['error'] = errors;
    } else {
      indicator.rowClass = "row2";
      indicator['progress'] = false;
      indicator['success'] = responseSuccess;
      indicator['error'] = errors;
    }
  }

  isResponsive(responsive:boolean){
    if(responsive){
      this.classContent = 'content_indicator_responsive';
    }else{
      this.classContent = 'content_indicator';
    }
  }
}
