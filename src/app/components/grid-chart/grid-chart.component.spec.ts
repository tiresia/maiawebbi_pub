import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridChartComponent } from './grid-chart.component';

describe('GridChartComponent', () => {
  let component: GridChartComponent;
  let fixture: ComponentFixture<GridChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
