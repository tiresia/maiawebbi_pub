import { Component, OnInit, Input } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-stack-chart',
  templateUrl: './stack-chart.component.html',
  styleUrls: ['./stack-chart.component.css']
})
export class StackChartComponent implements OnInit {

  @Input() data: any;
  @Input() height!: number;
  @Input() indicator: any;
  @Input() keys: any;
  label?:string;
  responsive:boolean = false;
  pixelsChart?:number;

  barChartOptions!: ChartOptions;
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = false;
  barChartPlugins = [pluginDataLabels as any];
  barChartData: ChartDataSets[] = [];

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(response => response.matches)
    );

  constructor(private breakpointObserver:BreakpointObserver) { }

  ngOnInit() {

    this.barChartOptions = {
      responsive: true,
      scales: {
        xAxes: [{
          stacked: true
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            callback: function (number, index, values) {
              if (isNaN(Number(number))) return null;
              if (number === null) return null;
              if (number === 0) return null;
              let abs = Math.abs(Number(number));
              const rounder = Math.pow(10, 1);
              const isNegative = number < 0;
              let key = '';

              const powers = [
                { key: 'Q', value: Math.pow(10, 15) },
                { key: 'T', value: Math.pow(10, 12) },
                { key: 'B', value: Math.pow(10, 9) },
                { key: 'M', value: Math.pow(10, 6) },
                { key: 'K', value: 1000 }
              ];

              for (let i = 0; i < powers.length; i++) {
                let reduced = abs / powers[i].value;
                reduced = Math.round(reduced * rounder) / rounder;
                if (reduced >= 1) {
                  abs = reduced;
                  key = powers[i].key;
                  break;
                }
              }
              return (isNegative ? '-' : '') + abs + key;
            }
          }
        }]
      },
      plugins: {
        datalabels: {
          formatter: (value, ctx) => {
            return "";
          },
        },
      }
    };

    this.keys.map((key:any) => {
      let object = {
        label:key,
        data:[] as any,
      }
      this.data.map((data:any)=>{
        if(key != this.indicator.titleField){
          if(Number(data[key])){
            object.data.push(data[key]);
          } 
        }
      });

      this.barChartData.push(object);
    });

    this.data.map((data:any) => {
      this.barChartLabels.push(data[this.indicator.titleField]);
    })

    this.label = this.indicator['titleField'];
    let widthWindow = screen.width;
    let heightWindow = screen.height;
    let pixels = 0;
    if (!this.responsive) {
      if (widthWindow > heightWindow) {
        pixels = (this.height / 100) * widthWindow;
        console.log(widthWindow);
      } else {
        console.log(heightWindow);
        pixels = (this.height / 100) * heightWindow;
      }
      this.pixelsChart = (pixels - 20);
    } else {
      if (widthWindow > heightWindow) {
        let result = (this.height / 100) * widthWindow;
        if (result > widthWindow) {
          pixels = result - widthWindow;
        } else {
          pixels = widthWindow - result;
        }
      } else {
        let result = (this.height / 100) * heightWindow;
        if (result > heightWindow) {
          pixels = result - heightWindow;
        } else {
          pixels = heightWindow - result;
        }
      }
      this.pixelsChart = (pixels + 20);
    }

  }

  isResponsive(responsive: boolean) {
    this.responsive = responsive;
  }

}
