import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  protocol: any = document.location.protocol;

  constructor(private http: HttpClient) { }    
  
  login(object:any){
    let params = new FormData();
    let arrUser = [] as any;
    arrUser = object.user.toString().split("@");
    params.append('D.u', arrUser[0])
    params.append('passwd', object.password);
    let url = "http://"+arrUser[1]+".tiresia.com.co/vg/gestion/apps/gsesies.cgi";
    return this.http.post(url, params, {responseType:'text'});
  }

  getProfile(object:any){
    let arrUser = [] as any;
    arrUser = object.username.toString().split("@");
    let url = "http://"+arrUser[1]+".tiresia.com.co/vg/gestion/apps/gsie.cgi?D.action=sie&siemodel=RHNOM&siefunction=096&dkuser="+object.codeUser+"&aleatorio=0.4612309201620519&sesid="+object.sesId+"&aseixer=0.47409528912976384&outformat=JSON"
    return this.http.post(url, '');
  }

}
