import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  constructor(private http: HttpClient) { }

  getGroups(object:any){
    let arrUser = [] as any;
    arrUser = object.username.toString().split("@");
    let url = "http://"+arrUser[1]+".tiresia.com.co/vg/gestion/apps/gsie.cgi?D.action=sie&siemodel=DSB&siefunction=001&perfiles=SI&dkperfil="+object.codeProfile+"&aleatorio=0&sesid="+object.sesId+"&aseixer=0.316921922378242";
    console.log('indicador1',url);
    return this.http.post(url, '',{responseType:'text'});
  }

  getIndicators(object:any){
    let arrUser = [] as any;
    arrUser = object.username.toString().split("@");
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    headers.set('Access-Control-Allow-Origin','*');
    headers.set('Access-Control-Allow-Methods','POST,GET,OPTIONS');
    headers.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    let url = "http://"+arrUser[1]+".tiresia.com.co/vg/gestion/apps/gsie.cgi?D.action=sie&siemodel=DSB&siefunction=005&aleatorio=0.3213974628597498&modo=test&tipo=" + object.type + "&bd=siebase&Dkindicador=" + object.dkIndicator + "&sesid=" + object.sesId + "&aseixer=0.11487543676048517";
    if (object.type == 'funcion' || object.type == 'consulta'){
      url = url+"&consulta="+object.consult;
    }
    console.log('indicador',url);
    return this.http.post(url,'',{headers, responseType:'text'});
  }
 
}
