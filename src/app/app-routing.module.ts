import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { PagesComponent } from './pages/pages/pages.component';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  {
    path:'',
    component: PagesComponent, 
    canActivate: [AuthGuard],
    children:[
      { path: 'home',canActivate: [AuthGuard], component: DashboardComponent, data: { text: 'Dashboard' }},
      { path: '', redirectTo: '/home', pathMatch: 'full'}
    ]
  },
  { path: 'login', component: LoginComponent, runGuardsAndResolvers:'always',data: { text: 'login' }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    TranslateModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
