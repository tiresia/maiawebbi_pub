import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { IndexComponent } from './pages/index/index.component';
import { FooterComponent } from './components/footer/footer.component';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ContentDashboardComponent } from './components/content-dashboard/content-dashboard.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { IgxCardModule, IgxGridModule, IgxDialogModule, IgxNavbarModule, IgxButtonModule, IgxRippleModule } from 'igniteui-angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PagesComponent } from './pages/pages/pages.component';
import { TabComponent } from './components/tabs/tab/tab.component';
import { DynamicTabsDirective } from 'src/app/components/tabs/tab/dynamic-tabs.directive';
import { TabsComponent } from './components/tabs/tabs.component';
import { PredashboardComponent } from './pages/predashboard/predashboard.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { GridChartComponent } from './components/grid-chart/grid-chart.component';
import { KeysPipe } from './keys.pipe';
import { StackChartComponent } from './components/stack-chart/stack-chart.component';
import { ChartLegendComponent } from './components/chart-legend/chart-legend.component';
import { DataBindingChartComponent } from './components/data-binding-chart/data-binding-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    IndexComponent,
    FooterComponent,
    BarChartComponent,
    DashboardComponent,
    ContentDashboardComponent,
    PieChartComponent,
    SpinnerComponent,
    PagesComponent,
    TabComponent,
    DynamicTabsDirective,
    TabsComponent,
    PredashboardComponent,
    PerfilComponent,
    GridChartComponent,
    KeysPipe,
    StackChartComponent,
    ChartLegendComponent,
    DataBindingChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ChartsModule,
    IgxCardModule,
    IgxGridModule,
    IgxDialogModule,
    IgxNavbarModule,
    IgxButtonModule,
    IgxRippleModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
