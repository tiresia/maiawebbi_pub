import { Component, OnInit, Input, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import {
  CloseScrollStrategy,
  ConnectedPositioningStrategy,
  HorizontalAlignment,
  IgxToggleDirective,
  VerticalAlignment
} from "igniteui-angular";

@Component({
  selector: 'app-predashboard',
  templateUrl: './predashboard.component.html',
  styleUrls: ['./predashboard.component.css']
})
export class PredashboardComponent implements OnInit {

  @ViewChild(IgxToggleDirective) public igxToggle?: IgxToggleDirective;
  @ViewChild("button") public igxButton?: ElementRef;
  
  @Input() groupsDashboard:any;
  @Input() indicatorsDashboard:any;
  @Input() typeGraphicsDashboard:any;
  @Input() responsiveFather?:boolean;

  tabs: any[] = [];
  classLabel: string = "container flex";
  classButton: string = "item_dash float-item";
  responsive:boolean = false;
  
  _positionSettings = {
    horizontalStartPoint: HorizontalAlignment.Left,
    verticalStartPoint: VerticalAlignment.Bottom
  };

  _overlaySettings = {
    closeOnOutsideClick: true,
    modal: false,
    positionStrategy: new ConnectedPositioningStrategy(this._positionSettings),
    scrollStrategy: new CloseScrollStrategy()
  };

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(response => response.matches)
    );
    
  constructor(
    private breakpointObserver: BreakpointObserver) { }

  ngOnInit() { 

    if(this.responsiveFather){
      this.classLabel = "container float";
    }
    let jsonGroups = JSON.parse(this.groupsDashboard);
    let jsonIndicators = JSON.parse(this.indicatorsDashboard);
    let jsonTypeGraphics = JSON.parse(this.typeGraphicsDashboard);
    
    jsonGroups.map((group:any, index:number)=>{
      let active = false;
      let classBtn = "item_dash float-item";
      if(index == 0){
        active = true;
        classBtn = "item_selected float-item";
      }
      let tab = {
        id:index,
        dk:group.Dkgrupo,
        label: group.nombregrupo,
        indicators:group.indicadores,
        time: group.timerefresh,
        active:active,
        class:classBtn
      }
      this.tabs.push(tab);
    });

      this.tabs.map((tab) => {
        for (let key in jsonIndicators) {
          let indicator = jsonIndicators[key];
          for(let index in tab.indicators){
            if (tab.indicators[index]['Dkindicador'] == indicator.Dkindicador) {
              if (indicator.tipofuente == '0'){
                tab.indicators[index]['type'] = 'consulta';
              }else{
                tab.indicators[index]['type'] = 'funcion';
              }
              let things = '\/';
              let consult = indicator.consulta.replace(things,"");
              consult = consult.replace(/(“|”)/g, "'"); 
              tab.indicators[index]['consult'] = consult.replace(/"/g, "'");
              tab.indicators[index]['titleField'] = indicator.titleField;
              tab.indicators[index]['valueField'] = indicator.valueField;
            }

            for (let keys in jsonTypeGraphics) {
              let typeGraphic = jsonTypeGraphics[keys];
              if (tab.indicators[index]['tipografica'] == typeGraphic.codigo) {
                tab.indicators[index]['tipografica'] = typeGraphic.nombre;
              }
            }
          }
        }

        
      });
  }

  isResponsive(responsive:boolean){
    this.responsive = responsive;
    if(responsive){
      this.classLabel = "container float"
    }else{
      this.classLabel = "container flex"
    }
  }

  activeTab(tab_selected:any){
    if(this.igxToggle){
      this.igxToggle.toggle();
    }
   
    this.tabs.map((tab)=>{
      if(tab_selected.dk == tab.dk){
        if(!tab.active){
          tab.active = true;
          tab.class = "item_selected float-item";
        }
      }else{
        tab.active = false;
        tab.class = "item_dash float-item";
      }
    });
  }

  toggle() {
    this._overlaySettings.positionStrategy.settings.target = this.igxButton?.nativeElement;
    this.igxToggle?.toggle(this._overlaySettings);
  }

}
