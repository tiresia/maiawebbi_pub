import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PredashboardComponent } from './predashboard.component';

describe('PredashboardComponent', () => {
  let component: PredashboardComponent;
  let fixture: ComponentFixture<PredashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PredashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PredashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
