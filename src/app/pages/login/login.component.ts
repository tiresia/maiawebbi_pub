import { Component, ElementRef, OnInit,ViewChild } from '@angular/core';
import { FormGroup,FormControl } from '@angular/forms';
import { IgxDialogComponent, OverlaySettings } from 'igniteui-angular';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { parseString } from 'xml2js';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('passwordView') passwordView:any;
  @ViewChild('alert') alert!: IgxDialogComponent;
  @ViewChild('dialog') dialog?: IgxDialogComponent;
  @ViewChild('myDiv') myDiv!: ElementRef;

  progress?:boolean;
  urlLogo!:string;
  urlLogoMobile!:string;
  language:string = "";
  username!:string;
  password!:string;
  messageError?:string;
  sendData:string[] = [];

  constructor(
    private route:Router,
    private translate:TranslateService,
    private loginService:LoginService) {
  }

  ngOnInit() {
    this.translate.use('es');
    this.urlLogo = "/assets/maia/logo/logo.png";
    this.urlLogoMobile = "/assets/maia/logo/logomobile.png"
  }

  nextFocus(event:any){
    console.log('2');
    if (event.key == 'Enter') {
      this.passwordView.nativeElement.focus();
    }
  }

  onKeydown(event:any){
    console.log('3');
    if (event.key == 'Enter') {
      this.login();
    }
  }

  selectChange(event: any) {
    console.log('4');
    this.translate.use(event.toLowerCase());
  }

  public openDialog() {
    let settings: OverlaySettings = {
        outlet: this.myDiv
    };
    this.alert.toggleRef.open(settings);
  }

  public closeDialog() {
    this.alert.toggleRef.close();
  }

  login(){
    this.progress = true;
    if(this.username && this.password){
      let object = {
        'user':this.username,
        'password':this.password
      }
      this.loginService.login(object).subscribe((response)=>{
        this.correctLogin(response);
      }, (error)=>{
          this.alert?.open();
          this.messageError = error.message;
      });
    }else{
      
      if(!this.username){
        this.alert?.open();
        this.messageError = this.translate.instant('login.noUser');
      }

      if(!this.password){
        this.alert?.open();
        this.messageError = this.translate.instant('login.noPass');
      }
    }
  }

  closeAlert() {
    if (this.alert) {
      this.progress = false;
      this.alert.close();
    }
  }

  private correctLogin(response: any) {
    let user:any;
    let error;
    parseString(response, function (err, result) {      
      if (result) {
        if(result.main.status == 'sesion'){
          user = {
            codeUser: result.main.dkuser,
            codeProfile: result.main.idperfil,
            profile: '',
            sesId: result.main.sesid,
            menu: result.main.tipomenu
          }
        }else{
          error = result.main.error;
        }
      }else{
        error = err;
      }
    });

    if(user){
      user.username=this.username;
      this.loginService.getProfile(user).subscribe((response:any) => {        
        if(response['status'] == 'ok'){
          let dk = response['Dk'];
          let data = response['record_empleado_' + dk];
          
          let firstName = '';
          let secondName = '';
          let firstLastName = '';
          let secondLastName = '';

          if (data.primernombre){
            firstName = data.primernombre;
          }

          if(data.segundonombre){
            secondName = data.segundonombre;
          }

          if (data.primerapellido){
            firstLastName = data.primerapellido;
          }

          if (data.segundoapellido){
            secondLastName = data.segundoapellido;
          }

          if (data.dkperfil){
            user.codeProfile = data.dkperfil;
          }

          let profile = {
            position: data.cargo_nombre,
            codePosition: data.cargoempleado,
            dtoCompany: data.dptoempresa_nombre,
            firstName: firstName,
            secondName: secondName,
            firstLastName: firstLastName,
            secondLastName: secondLastName
          };

          if (profile) {
            user.profile = profile;
            localStorage.setItem('user', JSON.stringify(user));
            this.route.navigate(['/home']);
          }
        }else{
            if(response['error'] && this.alert){
              console.log('Enter4');
              this.alert.open();
              this.messageError = response['error'];
            }
        }
      },
      (error)=>{
        console.log('Enter5');
        if(this.alert){
          this.alert.open();
          this.messageError = error;
        }
        
      });
     
    }else if(error){
      console.log('Enter6');
      if(this.alert){
        this.alert.open();
        this.messageError = error;
      }
    }
    
  }

}
