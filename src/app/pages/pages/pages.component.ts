import { Component, ViewChild, ComponentFactoryResolver, QueryList, ContentChildren, HostBinding, OnInit } from '@angular/core';
import {
  IgxNavigationDrawerComponent,
  IgxDialogComponent,
  HorizontalAlignment,
  VerticalAlignment,
  CloseScrollStrategy,
  ConnectedPositioningStrategy,
  IgxDropDownComponent
} from 'igniteui-angular';
import { DynamicTabsDirective } from 'src/app/components/tabs/tab/dynamic-tabs.directive';
import { environment } from '../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { RepositoryService } from 'src/app/services/repository.service';
import { parseString } from 'xml2js';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { ChangeDetectorRef } from '@angular/core';
import { TabComponent } from 'src/app/components/tabs/tab/tab.component';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

  @ViewChild('drawer') drawer?:IgxNavigationDrawerComponent;
  @ViewChild('dialogLogout') dialogLogout?:IgxDialogComponent;
  @ViewChild(DynamicTabsDirective) dynamicTabPlaceholder!: DynamicTabsDirective;
  @ContentChildren(TabComponent) tabs?: QueryList<TabComponent>;
  @ViewChild('dashboard') dashboard:any;
  @ViewChild('settings') settings?:IgxDialogComponent;
  @ViewChild('dialogError') dialogError?:IgxDialogComponent;
  @ViewChild('dialogProfile') dialogProfile?:IgxDialogComponent;

  activeMenu:boolean = true;
  urlProfile: string = "/assets/maia/perfil/imgperfil.png";
  urlLogo: string = "/assets/maia/logo/logomobile.png";
  urlMobile: string = "/assets/maia/logo/logomobile.png";
  items:any[] = [];
  arrayForm:any;
  dynamicTabs: TabComponent[] = [];
  tab?: TabComponent;
  arrayMenuItems:any[] = [];
  typeMenu!:boolean;
  selectedTheme: any;
  theme:any;
  responsive:boolean = false;
  profile:any;
  user:any;
  msgError?:string;
  groupsDashboard:any;
  indicatorsDashboard:any;
  typeGraphicsDashboard:any;
  loadDashboard:boolean = true;
  errorGroups:boolean = false;
  classContent: string = 'content-wrap';

  THEME = {
    LIGHT: "ocean-theme",
    DARK: "sky-theme",
    BLACK: "forest-theme"
  };

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(response => response.matches)
    );

  @HostBinding("class")
  public themesClass: string = this.THEME.LIGHT;

  private _dropDownPositionSettings = {
    horizontalStartPoint: HorizontalAlignment.Left,
    verticalStartPoint: VerticalAlignment.Bottom
  };
  
  private _dropDownOverlaySettings = {
    closeOnOutsideClick: true,
    modal: false,
    positionStrategy: new ConnectedPositioningStrategy(this._dropDownPositionSettings),
    scrollStrategy: new CloseScrollStrategy()
  };

  constructor(
    private translate:TranslateService,
    private _componentFactoryResolver: ComponentFactoryResolver,
    private repository:RepositoryService,
    private breakpointObserver: BreakpointObserver,
    private cdRef: ChangeDetectorRef) {
    this.arrayMenuItems.push({ "id": 1, "value": true, "name": "Vertical" }, { "id": 2, "value": false, "name": "Horizontal" });
    this.obtainTheme();
    this.responsiveMobile();
    this.user = JSON.parse(localStorage.getItem('user')!);
    this.profile = this.user.profile;
    this.profile.image = this.urlProfile;
  }

  obtainTheme(){
    this.theme = localStorage.getItem('theme');
    if(this.theme){
      this.themesClass = this.theme;
    }
  }

  responsiveMobile() {
    let width = screen.width;
    if (width < 729) {
      localStorage.setItem("menu", "true");
      this.responsive = true;
      this.activeMenu = true;
    }
  }

  ngOnInit(): void {
    this.translate.use('es');
    if (screen.width < 729){
      localStorage.setItem("menu", "true");
      this.responsive = true;
      this.activeMenu = true;
      this.classContent = 'content_responsive'; 
    }
    let subItems =[];
    let subItems2 = [];

    let subItem2 ={
      id:2,
      name:'dashBoard',
    }
    subItems2.push(subItem2);
    
    let subItem = {
      id:1,
      name:'config',
      sons:subItems2
    }
    subItems.push(subItem);
    
    let item = {
      name:'home',
      active:true,
      sons:subItems
    }
    this.items.push(item);
    this.loadGroups(); 
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  loadGroups(){
    let data = {
      codeProfile:this.user.codeProfile,
      sesId:this.user.sesId,
      username:this.user.username
    };

    this.repository.getGroups(data).subscribe((response)=>{
      if(response){
          this.parseXml(response);
      }else{
        this.dialogError?.open();
        this.msgError = this.translate.instant('noResponse');
      }
    },
    (error)=>{  
      this.dialogError?.open();
      this.msgError = error.message;
    });
  }

  parseXml(response:any){

    let groups = [];
    let indicators = [];
    let typeGraphics = [];
    
    let jsonGroups:any;
    let jsonIndicators:any;
    let jsonTypeGraphics:any;

    let errors:any;

    parseString(response, function (error:any, result:any) {
      if (result) {
        if (result.main.status == 'ok') {
          if(result.main.gruposdashboard){
            jsonGroups = JSON.parse(result.main.gruposdashboard);
          }

          if (result.main.indicadores){
            jsonIndicators = JSON.parse(result.main.indicadores);
          }
         
          if (result.main.tipografica){
            jsonTypeGraphics = JSON.parse(result.main.tipografica);
          }
        } else if (result.main.status == 'error'){
          errors = result.main.error;
        }
      } else {
        errors = error;
      }
    });
    
    if (!errors){
      if (jsonGroups) {
        for (let key in jsonGroups) {
          groups.push(jsonGroups[key]);
        }
      }

      if (jsonIndicators) {
        for (let key in jsonIndicators) {
          indicators.push(jsonIndicators[key][0]);
        }
      }

      if (jsonTypeGraphics) {
        for (let key in jsonTypeGraphics) {
          typeGraphics.push(jsonTypeGraphics[key]);
        }
      }

      if (groups.length > 0) {
        this.groupsDashboard = JSON.stringify(groups);
      }

      if (indicators.length > 0) {
        this.indicatorsDashboard = JSON.stringify(indicators);
      }

      if (typeGraphics.length > 0) {
        this.typeGraphicsDashboard = JSON.stringify(typeGraphics);
      }

      if (this.groupsDashboard && this.indicatorsDashboard && this.typeGraphicsDashboard) {
        this.loadDashboard = false;
      }
    }else{
      this.errorGroups = true;
      this.dialogError?.open();
      this.msgError = errors;
    }
  }

  ngAfterContentInit(): void {
    this.openDashboard();
    if (localStorage.getItem("menu") == "true") {
      this.activeMenu = true;
    } else {
      this.activeMenu = false;
    }
  }

  openDashboard(){

    let tabTitle = this.translate.instant('dashBoard');
    let tabFather = this.translate.instant('config');
    let tabSon = this.translate.instant('home');
    let stringBreadCrumbs = tabSon + ">";
    

    this.arrayForm = {
      "title": tabTitle,
      "id": 2,
      "breadCums": tabFather + ">" + stringBreadCrumbs + tabTitle,
      "name": tabTitle
    };
    
    this.openTab(this.dashboard, this.arrayForm, true, 2)
  }

  logout() {
    if (this.dialogLogout) {
      this.dialogLogout.open();
    }
  
  }

  closeSession(){
    localStorage.clear();
    window.location.replace("/login");
  }
 
  onOpenTabDom(title:string, idForm:number, nameFather:string, nameSon:string){
    console.log(nameFather);
    console.log(nameSon);
    let tabTitle = this.translate.instant(title);
    let tabFather = this.translate.instant(nameFather);
    let stringBreadCrumbs;
    if(nameSon){
      let tabSon = this.translate.instant(nameSon);
      stringBreadCrumbs = tabSon + ">";
    }
    
    this.arrayForm = {
      "title": tabTitle,
      "id": idForm,
      "breadCums": tabFather + ">" + stringBreadCrumbs + tabTitle,
      "name": tabTitle
    };

    if(this.drawer){
      this.drawer.close();
    }

    this.openTab(this.dashboard, this.arrayForm, true, idForm)
  }

  openTab(template:any, data:any, isClosed:boolean, idTab:number){    
    let limitTabs = environment.limitTabs - 1;
    if (this.dynamicTabs.length > limitTabs) {
    } else {
      let idExist = false;
      if (this.dynamicTabs.length > 0) {
        for (let id of this.dynamicTabs) {  
          if (id["idTab"] == idTab) {
            idExist = true;
            this.tabs?.toArray().forEach(tab => (tab.active = false));
            this.dynamicTabs.forEach(tab => (tab.active = false));
            id["active"] = true;
            return;
          }
        }
      }

      if (!idExist && this.dynamicTabPlaceholder) {        
        const componentFactory = this._componentFactoryResolver.resolveComponentFactory(TabComponent);
        const viewContainerRef = this.dynamicTabPlaceholder.viewContainer;
        const componentRef = viewContainerRef.createComponent(componentFactory);
        const instance: TabComponent = componentRef.instance as TabComponent;
        instance.title = data.title;
        instance.template = template;
        instance.dataContext = data;
        instance.isCloseable = isClosed;
        instance["idTab"] = idTab;
        this.dynamicTabs.push(componentRef?.instance as TabComponent);
        this.selectTab(this.dynamicTabs[this.dynamicTabs.length - 1]);
      }
    }
  }

  selectTab(tab: TabComponent) {
    this.tabs?.toArray().forEach(tab => (tab.active = false));
    this.dynamicTabs.forEach(tab => (tab.active = false)); 
    tab.active = true;
    this.tab = tab;
  }

  openSettings(){
    if(this.drawer){
      if(this.drawer.isOpen){
 
        if (this.settings) {
          this.settings.open();
        }
      }else{

        if (this.settings) {
          this.settings.open();
        }
      }
    }else{
      if (this.settings) {
        this.settings.open();
      }
    }
  }


  toggleDropDownMenu(eventArgs: any, selectedDropDown: IgxDropDownComponent) {
    if (selectedDropDown) {
      const dropDown = selectedDropDown;
      this._dropDownOverlaySettings.positionStrategy.settings.target = eventArgs.target;
      dropDown.toggle(this._dropDownOverlaySettings);
    }
  }

  toggleDropDown(eventArgs:any, selectedDropDown: IgxDropDownComponent) {
    if (selectedDropDown) {
      const dropDown = selectedDropDown;
      this._dropDownOverlaySettings.positionStrategy.settings.target = eventArgs.target;
      dropDown.toggle(this._dropDownOverlaySettings);
    }
  }
  
  changeMenu(e: any) {
    if (e.value) {
      this.typeMenu = true;
    } else {
      this.typeMenu = false
    }
  }

  selectTheme(theme:any){
    this.selectedTheme = theme;
  }

  actionOk(){
    this.settings?.close();
    localStorage.setItem("menu", String(this.typeMenu));
    this.activeMenu = this.typeMenu;
    if (this.selectedTheme) {
      this.themesClass = this.selectedTheme;
      localStorage.setItem("theme", this.selectedTheme);
    }
  }

  openProfile(){
    this.dialogProfile?.open();
  }

  closeError(){
    if(this.dialogError){
      if(this.dialogError.isOpen){
        this.dialogError.close();
      }
    }
  }
  
  reloadGroups(){
    this.errorGroups = false;
    this.loadDashboard = true;
    this.loadGroups();
  }

  isResponsive(responsive: boolean) {
    this.responsive = responsive;
    if(responsive){
      this.classContent = 'content_responsive'; 
      this.activeMenu = true;
    }else{
      this.classContent = 'content-wrap';
      this.activeMenu = false;
    }
  }


}
